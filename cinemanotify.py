import os
import sys
from datetime import date, timedelta
from urllib.parse import quote

import requests
import yagmail
from bs4 import BeautifulSoup

BASE = os.path.dirname(os.path.abspath(__file__))
SEEN = os.path.join(BASE, 'seen.txt')
SUBSCRIBERS = os.path.join(BASE, 'subs.txt')
BASE_URL = 'https://www.clevelandcinemas.com/includes/load_data?load=house_showtimes&house_id={house_id}&date={date}'


def main():
    if len(sys.argv) != 2:
        print('Usage: python3 cinemanotify.py [house_id]')
        sys.exit(1)
    house_id = sys.argv[1]
    print('Getting movies for house {}...'.format(house_id))
    tomorrow = date.today() + timedelta(days=1)
    url = BASE_URL.format(house_id=house_id, date=tomorrow.strftime('%x'))
    marquee = requests.get(url).text
    now_showing = parse_page(marquee)
    print('\n{} movies now playing:'.format(len(now_showing)))
    print('\n'.join(now_showing), end='\n\n')
    print('Checking for new movies...')
    seen_movies = get_seen()
    new_movies = now_showing.difference(seen_movies)
    if not new_movies:
        print('No new movies. Exiting.')
        sys.exit(0)
    print('{} new movie(s) found.'.format(len(new_movies)))
    email_body = get_movie_info(new_movies)
    print(email_body)
    yag = get_yag()
    subject = '{} new movie{} on {}!'.format(len(new_movies), 's' if len(new_movies) > 1 else '',
                                             date.today().strftime('%x'))
    print('Sending emails...')
    for recipient in get_subscribers():
        try:
            yag.send(to=recipient, subject=subject, contents=email_body)
        except Exception:
            print('Error sending to recipient {!r}'.format(recipient))
    print('Marking these movies as watched...')
    add_seen(new_movies)
    print('Done!')


def get_yag(user=None, password=None):
    try:
        yag = yagmail.SMTP()
    except(RuntimeError, FileNotFoundError):  # raised due to lack of UN/password
        if user is None:
            user = input('Enter username: ').strip()
        if password is None:
            password = input('Enter password: ')
        yag = yagmail.SMTP(user=user, password=password)
    return yag


def get_seen():
    try:
        with open(SEEN) as f:
            return set(m.strip() for m in f.readlines())
    except FileNotFoundError:
        return set()


def add_seen(titles):
    with open(SEEN, 'a') as f:
        f.write('\n'.join(titles) + '\n')


def get_subscribers():
    try:
        with open(SUBSCRIBERS) as f:
            return set(n for n in (l.strip() for l in f.readlines()) if n)
    except FileNotFoundError:
        return set()


def parse_page(page):
    soup = BeautifulSoup(page, 'html.parser')
    titles = set()
    for title in soup.find_all(class_='movie-name'):
        titles.add(title.contents[0].strip())
    return titles


def get_movie_info(movies):
    text = []
    for movie in movies:
        text.append('{name}\nWikipedia: https://en.wikipedia.org/wiki/Special:Search?search={safe_name}&go=Go\n'
                    'IMDB: https://www.imdb.com/find?s=all&q={safe_name}'.format(name=movie, safe_name=quote(movie)))
    if len(movies) == 1:
        greeting = "Hiya! There's a new movie showing:"
    else:
        greeting = 'Heya! There are {} new movies showing:'.format(len(movies))
    return greeting + '\n\n' + '\n\n'.join(text)


if __name__ == '__main__':
    main()
